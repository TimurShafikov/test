﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Xml.Linq;
using WpfApp1.Model;
using WpfApp1.View;

namespace WpfApp1.ViewModel
{
    public class MainWindowViewModel : DependencyObject, INotifyPropertyChanged
    {
        private ObservableCollection<Amclist> _Amclist; 

        public ObservableCollection<Amclist> Amclist
        {
            get => _Amclist;
            set
            {
                _Amclist = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<ProductInfo> _Product2; 

        public ObservableCollection<ProductInfo> Products2
        {
            get => _Product2;
            set
            {
                _Product2 = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<ProductInfo> _Product;
        public ObservableCollection<ProductInfo> Products
        {
            get => _Product;
            set
            {
                _Product = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<ProducerInfo> _Producer;
        public ObservableCollection<ProducerInfo> Producers
        {
            get => _Producer;
            set
            {
                _Producer = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<HistoryList> _history;
        public ObservableCollection<HistoryList> History
        {
            get => _history;
            set
            {
                _history = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Warehouse> Warehouse { get; set; }

        private ProductInfo selectedProduct;
        public ProductInfo SelectedProduct
        {
            get => selectedProduct;
            set
            {
                selectedProduct = value;
                if (value != null)
                {
                    Products = new ObservableCollection<ProductInfo>();
                    Producers = new ObservableCollection<ProducerInfo>();
                    Amclist = new ObservableCollection<Amclist>();
                    XDocument xdoc = XDocument.Load("GetTest.xml");
                    //XNamespace wbNs = "http://fsrar.ru/WEGAIS/TTNSingle_v3";
                    IEnumerable<XElement> elements = xdoc.Descendants("Position").Where(x => x.Element("Product").Element("ShortName").Value == value.ShortName);

                    var prod = new ProductInfo();
                    var prod2 = new ProducerInfo();
                    var prod3 = new Amclist();

                    foreach (XElement e in elements)
                    {
                        prod.FullName = e.Element("Product").Element("FullName").Value;
                        prod.UnitType = "Тип упаковки:   " + e.Element("Product").Element("UnitType").Value;
                        prod.Type = "Тип:   " + e.Element("Product").Element("Type").Value;
                        prod.AlcCode = "Код алко:   " + e.Element("Product").Element("AlcCode").Value;
                        prod.Capacity = "Объем:   " + e.Element("Product").Element("Capacity").Value + "л.";
                        prod.AlcVolume = "Содержание спирта:   " + e.Element("Product").Element("AlcVolume").Value + "%";
                        prod.ProductVCode = "Код классификации:   " + e.Element("Product").Element("ProductVCode").Value;

                        prod.Quantity = "Количество:   " + e.Element("Quantity").Value;
                        prod.Price = "Цена:   " + e.Element("Price").Value + "₽";

                        prod2.ShortName = e.Element("Product").Element("Producer").Element("UL").Element("ShortName").Value;
                        prod2.FullName = e.Element("Product").Element("Producer").Element("UL").Element("FullName").Value;
                        prod2.INN = "ИНН:   " + e.Element("Product").Element("Producer").Element("UL").Element("INN").Value;
                        prod2.KPP = "КПП:   " + e.Element("Product").Element("Producer").Element("UL").Element("KPP").Value;

                        prod2.Country = "Код страны:   " + e.Element("Product").Element("Producer").Element("UL").Element("address").Element("Country").Value;
                        prod2.RegionCode = "Код региона:   " + e.Element("Product").Element("Producer").Element("UL").Element("address").Element("RegionCode").Value;
                        prod2.Description = "Адресс:   " + e.Element("Product").Element("Producer").Element("UL").Element("address").Element("description").Value;

                        foreach (XElement e1 in elements.Descendants("amc"))
                        {
                            prod3.Amc = e1.Value;
                            Amclist.Insert(0, prod3);
                        }

                        Products.Insert(0, prod);
                        Producers.Insert(0, prod2);

                        _Quantity = Convert.ToInt32(e.Element("Quantity").Value);
                        PropertyChanged.Invoke(this, new PropertyChangedEventArgs("EditQuantity"));

                    }
                }
                OnPropertyChanged();
            }
        }

        void ClearHistory()
        {
            XDocument hdoc = XDocument.Load("HistoryTest.xml");
            IEnumerable<XElement> aux = hdoc.Descendants("Product");

            List<HistoryList> his = new List<HistoryList>();
            var his1 = new HistoryList();
            foreach (XElement e in aux)
            {
                his1.Name = e.Element("Name").Value;
                his1.Date = e.Element("Date").Value;
                his1.Price = e.Element("Price").Value;
                his1.Quantity = e.Element("Quantity").Value;
                his.Add(his1);

            }
            History = new ObservableCollection<HistoryList>(his);
        }

        public MainWindowViewModel()
        {
            GetNames();
            ClearHistory();
        }

        public void GetNames()
        {
            Products2 = new ObservableCollection<ProductInfo>();
            XDocument xdoc = XDocument.Load("GetTest.xml");
            IEnumerable<XElement> elements = xdoc.Descendants("Position");
            

            foreach (XElement e in elements)
            {
                XElement nameElement = e.Element("Product").Element("ShortName");
                XElement QuantityElement = e.Element("Quantity");

                if (nameElement != null)
                {
                    var prod = new ProductInfo();
                    prod.ShortName = nameElement.Value;
                    prod.Quantity = QuantityElement.Value;
                    Products2.Insert(0, prod);
                }
            }

        }

        private ICommand _SellClick;
        public ICommand SellClick => _SellClick ?? (_SellClick = new RelayCommand(() => SellGoods(), () => CanExecute));

        public bool CanExecute => true|false;

        private string _QuantityAll;
        private string _MiddleCost;
        private string _StartDate;
        private string _LastDate;

        public string Stat1
        {
            get => _QuantityAll;
            set
            {
                _QuantityAll = value;
                OnPropertyChanged();
            }
        }

        public string Stat2
        {
            get => _MiddleCost;
            set
            {
                _MiddleCost = value;
                OnPropertyChanged();
            }
        }

        public string Stat3
        {
            get => _StartDate;
            set
            {
                _StartDate = value;
                OnPropertyChanged();
            }
        }

        public string Stat4
        {
            get => _LastDate;
            set
            {
                _LastDate = value;
                OnPropertyChanged();
            }
        }


        private string _Name;
        private int _QuantityAC;
        private float _Cost;
        private int _VAT;

        public string EditName
        {
            get => _Name;
            set
            {
                _Name = value;
                OnPropertyChanged();
            }
        }

        public int EditQuantityAC
        {
            get => _QuantityAC;
            set
            {
                _QuantityAC = value;
                OnPropertyChanged();
            }
        }

        public float EditCost
        {
            get => _Cost;
            set
            {
                _Cost = value;
                OnPropertyChanged();
            }
        }

        public int EditVAT
        {
            get => _VAT;
            set
            {
                _VAT = value;
                OnPropertyChanged();
            }
        }

        private int _Quantity;
        public int EditQuantity
        {
            get => _Quantity;
            set
            {
                if (selectedProduct != null)
                {
                    if (value <= Convert.ToInt32(selectedProduct.Quantity))
                        _Quantity = value;
                    else
                        _Quantity = Convert.ToInt32(selectedProduct.Quantity);
                }
                OnPropertyChanged();
            }
        }


        public void SellGoods()
        {
            if (_Quantity != 0 && selectedProduct != null)
            {
                XDocument hdoc = XDocument.Load("HistoryTest.xml");
                XElement aux = hdoc.Element("Products");

                XDocument xdoc = XDocument.Load("GetTest.xml");
                IEnumerable<XElement> root = xdoc.Descendants("Position");

                foreach (XElement e in root)
                {
                    if (e.Element("Product").Element("ShortName").Value == selectedProduct.ShortName)
                    {
                        e.Element("Quantity").Value = (Convert.ToInt32(e.Element("Quantity").Value) - _Quantity).ToString();
                        xdoc.Save("GetTest.xml");
                        
                        aux.Add(new XElement("Product",
                            new XElement("Name", e.Element("Product").Element("ShortName").Value),
                            new XElement("Date", DateTime.UtcNow),
                            new XElement("Price", (Convert.ToDouble(e.Element("Price").Value) * Convert.ToInt32(e.Element("Quantity").Value)).ToString()),
                            new XElement("Quantity", _Quantity.ToString())));
                        hdoc.Save("HistoryTest.xml");

                        _Quantity = Convert.ToInt32(e.Element("Quantity").Value);
                        PropertyChanged.Invoke(this, new PropertyChangedEventArgs("EditQuantity"));
                    }
                }
            }
        }
        

        private string _FilterText;
        public string FilterText
        {
            get => _FilterText;
            set
            {
                _FilterText = value;

                OnPropertyChanged();

                Products2 = new ObservableCollection<ProductInfo>();

                XDocument xdoc = XDocument.Load("GetTest.xml");
                IEnumerable<XElement> elements = xdoc.Descendants("Position");
                foreach (XElement e in elements)
                {
                    XElement nameElement = e.Element("Product").Element("ShortName");

                    if (nameElement != null && nameElement.Value.Contains(value))
                    {
                        var prod = new ProductInfo();
                        prod.ShortName = nameElement.Value.ToString();
                        Products2.Insert(0, prod);
                    }
                }

                Products = new ObservableCollection<ProductInfo>();
                Producers = new ObservableCollection<ProducerInfo>();
                Amclist = new ObservableCollection<Amclist>();
                return;

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
