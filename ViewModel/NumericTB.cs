﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfApp1.ViewModel
{
    public class NumericTB : TextBox
    {
        private static readonly Regex regex = new Regex("[0-9]");

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (!regex.IsMatch(e.Key.ToString())) e.Handled = true;
            base.OnKeyDown(e);
        }
    }
}
