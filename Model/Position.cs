namespace WpfApp1.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Position")]
    public partial class Position
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public int InformF2Id { get; set; }

        public int Quantity { get; set; }

        public string Price { get; set; }

        public int Identity { get; set; }

        public string FARegId { get; set; }

        public virtual InformF2 InformF2 { get; set; }

        public virtual Product Product { get; set; }
    }
}
