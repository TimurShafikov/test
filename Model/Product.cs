namespace WpfApp1.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            Position = new HashSet<Position>();
        }

        public int Id { get; set; }

        public int ProducerId { get; set; }

        [Required]
        [StringLength(200)]
        public string UnitType { get; set; }

        [Required]
        [StringLength(200)]
        public string Type { get; set; }

        [Required]
        [StringLength(200)]
        public string FullName { get; set; }

        [Required]
        [StringLength(200)]
        public string ShortName { get; set; }

        [Required]
        [StringLength(200)]
        public string AlcCode { get; set; }

        public string Capacity { get; set; }

        public int AlcVolume { get; set; }

        public int ProductVCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Position> Position { get; set; }

        public virtual Producer Producer { get; set; }
    }
}
