namespace WpfApp1.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("History")]
    public partial class History
    {
        public int Id { get; set; }

        public int GoodId { get; set; }

        public DateTime Date { get; set; }

        public int Quantity { get; set; }

        public float Cost { get; set; }

        public int VAT { get; set; }

        public virtual Goods Goods { get; set; }
    }
}
