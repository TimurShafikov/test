namespace WpfApp1.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Store : DbContext
    {
        public Store()
            : base("name=Store")
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Amclist> Amclist { get; set; }
        public virtual DbSet<Cost> Cost { get; set; }
        public virtual DbSet<Goods> Goods { get; set; }
        public virtual DbSet<History> History { get; set; }
        public virtual DbSet<InformF2> InformF2 { get; set; }
        public virtual DbSet<Position> Position { get; set; }
        public virtual DbSet<Producer> Producer { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Warehouse> Warehouse { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .HasMany(e => e.Producer)
                .WithRequired(e => e.Address)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Amclist>()
                .HasMany(e => e.InformF2)
                .WithRequired(e => e.Amclist)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cost>()
                .HasMany(e => e.Goods)
                .WithRequired(e => e.Cost)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Goods>()
                .HasMany(e => e.History)
                .WithRequired(e => e.Goods)
                .HasForeignKey(e => e.GoodId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InformF2>()
                .HasMany(e => e.Position)
                .WithRequired(e => e.InformF2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Producer>()
                .HasMany(e => e.Product)
                .WithRequired(e => e.Producer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.Position)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.Goods)
                .WithRequired(e => e.Warehouse)
                .HasForeignKey(e => e.WHId)
                .WillCascadeOnDelete(false);
        }
    }
}
