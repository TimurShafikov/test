﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.View
{
    public class ProducerInfo
    {
        public string ClientRegId { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string INN { get; set; }
        public string KPP { get; set; }

        public string Country { get; set; }
        public string RegionCode { get; set; }
        public string Description { get; set; }
    }
}
