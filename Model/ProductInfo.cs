﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.View
{
    public class ProductInfo
    {
        public string FullName { get; set; }
        public string UnitType { get; set; }
        public string Type { get; set; }
        public string AlcCode { get; set; }
        public string Capacity { get; set; }
        public string AlcVolume { get; set; }

        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Identity { get; set; }
        public string FARegId { get; set; }

        public string ProductVCode { get; set; }

        public string ShortName { get; set; }
        public string ClientRegId { get; set; }
        public string Description { get; set; }
        
    }
}
